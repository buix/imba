import * as raf from 'raf';

export default () => {
  raf.polyfill(globalThis);

  if (typeof globalThis.requestIdleCallback !== 'function') {
    globalThis.requestIdleCallback = globalThis.requestAnimationFrame.bind(globalThis);
  }
};
