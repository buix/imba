export default (nuxt, inject) => {
  const mettEvents = new Map();
  inject('bus', {
    on(eventName, callback) {
      const set = mettEvents.get(eventName);
      if (set) {
        set.add(callback);
      } else {
        mettEvents.set(eventName, new Set([callback]));
      }
    },
    off(eventName, callback) {
      const set = mettEvents.get(eventName);
      if (set) {
        set.delete(callback);
      }
    },
    emit(eventName, e) {
      const setName = mettEvents.get(eventName);
      if (setName) {
        setName.forEach((callback) => {
          callback(e, eventName);
        });
      }
      const setStar = mettEvents.get('*');
      if (setStar) {
        setStar.forEach((callback) => {
          callback(e, eventName);
        });
      }
    }
  });
};
