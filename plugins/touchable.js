import Vue from 'vue';

const onTouch = () => {
  return true;
};
const param = { capture: true, passive: true };
Vue.directive('touchable', {
  bind(el) {
    el.addEventListener('touchstart', onTouch, param);
  },
  unbind(el) {
    el.removeEventListener('touchstart', onTouch, param);
  }
});
