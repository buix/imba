export default ({ app }, inject) => {
  let param = false;
  try {
    const opts = Object.defineProperty({}, 'passive', {
      get() {
        param = { passive: true };
        return true;
      }
    });
    globalThis.addEventListener('passive', null, opts);
    globalThis.removeEventListener('passive', null, opts);
  } catch { }
  inject('passive', param);

  let scroll = false;
  globalThis.addEventListener('scroll', (e) => {
    if (!scroll) {
      globalThis.requestAnimationFrame(() => {
        app.$bus.emit('scroll', e);
        scroll = false;
      });

      scroll = true;
    }
  }, param);

  let resize = false;
  globalThis.addEventListener('resize', (e) => {
    if (!resize) {
      globalThis.requestAnimationFrame(() => {
        app.$bus.emit('resize', e);
        resize = false;
      });

      resize = true;
    }
  }, param);

  const originalAdd = globalThis.addEventListener.bind(globalThis);
  globalThis.addEventListener = (name, listener, params) => {
    if (name === 'scroll' || name === 'resize') {
      app.$bus.on(name, listener);
    } else {
      originalAdd(name, listener, params);
    }
  };

  const originalRemove = globalThis.removeEventListener.bind(globalThis);
  globalThis.removeEventListener = (name, listener, params) => {
    if (name === 'scroll' || name === 'resize') {
      app.$bus.off(name, listener);
    } else {
      originalRemove(name, listener, params);
    }
  };
};
