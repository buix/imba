const context = (() => {
  if (typeof globalThis !== 'undefined') {
    return globalThis;
  }
  if (typeof self !== 'undefined') {
    return self;
  }
  if (typeof window !== 'undefined') {
    return window;
  }
  if (typeof global !== 'undefined') {
    return global;
  }
  if (typeof this !== 'undefined') {
    return this;
  }
  // eslint-disable-next-line no-new-func
  return Function('return this')();
})();

if (!('globalThis' in context)) {
  context.globalThis = context;
}
