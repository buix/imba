export default (nuxt, inject) => {
  const detect = {};

  if (process.server) {
    let userAgent = '';

    if (typeof nuxt.req !== 'undefined') {
      userAgent = nuxt.req.headers['user-agent'];
    }

    // detect.rich = userAgent.indexOf("Edge") === -1;
    detect.nt = userAgent.indexOf('NT') !== -1;
  }

  if (process.client || process.browser) {
    const ctx = globalThis;

    const userPlatform = String(ctx.navigator.platform).toLocaleLowerCase();

    // detect.rich = ctx.InstallTrigger || ctx.ApplePaySession || ctx.navigator.webkitPersistentStorage;
    detect.nt = userPlatform.indexOf('win') !== -1;
  }

  inject('detect', detect);
};
