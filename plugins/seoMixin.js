import Vue from 'vue';

export default ({ route }) => {
  Vue.mixin({
    head() {
      return {
        title: this.seoTitle,
        link: [
          {
            rel: 'canonical',
            href: route.path
          }
        ],
        meta: [
          {
            hid: 'description',
            name: 'description',
            content: this.seoDesc
          },
          {
            property: 'og:title',
            content: this.seoTitle
          },
          {
            property: 'og:description',
            content: this.seoDesc
          },
          {
            property: 'og:image',
            content: this.seoImg
          },
          {
            property: 'twitter:card',
            content: 'summary'
          },
          {
            property: 'twitter:title',
            content: this.seoTitle
          },
          {
            property: 'twitter:description',
            content: this.seoDesc
          },
          {
            property: 'twitter:image',
            content: this.seoImg
          }
        ]
      };
    }
  });
};