import { fetchOptions, fetchPages, fetchFormats, fetchThemes } from '~/api/index';
import { clearLink, formatInnerNumber } from '~/utils/data';
import { OPTIONS_MAP_SCHEME, OPTIONS_VALUE_WHY, OPTIONS_VALUE_PREPARE, OPTIONS_VALUE_WORK } from '~/utils/constants';

export const state = () => ({
  links: {},
  active: {},
  value: {
    why: [],
    prepare: [],
    work: []
  },
  pages: null,
  formats: null,
  themes: null
});

export const mutations = {
  UPDATE_OPTIONS(state, payload) {
    Object.keys(payload).forEach((key) => {
      state.active[key] = payload[key].is_active;
      if (key in OPTIONS_MAP_SCHEME) {
        state.links[key] = {
          value: payload[key].value,
          url: `${OPTIONS_MAP_SCHEME[key]}${clearLink(payload[key].value)}`
        };
      }
      if (key in OPTIONS_VALUE_WHY) {
        state.value.why[OPTIONS_VALUE_WHY[key]] = formatInnerNumber(payload[key].value);
      }
      if (key in OPTIONS_VALUE_PREPARE) {
        state.value.prepare[OPTIONS_VALUE_PREPARE[key]] = formatInnerNumber(payload[key].value);
      }
      if (key in OPTIONS_VALUE_WORK) {
        state.value.work[OPTIONS_VALUE_WORK[key]] = formatInnerNumber(payload[key].value);
      }
    });
  },
  UPDATE_PAGES(state, payload) {
    state.pages = payload;
  },
  UPDATE_FORMATS(state, payload) {
    state.formats = payload;
  },
  UPDATE_THEMES(state, payload) {
    state.themes = payload;
  }
};

export const actions = {
  async FETCH_THEMES({ commit }) {
    commit('UPDATE_THEMES', await fetchThemes(this));
  },
  async FETCH_FORMATS({ commit }) {
    commit('UPDATE_FORMATS', await fetchFormats(this));
  },
  async FETCH_OPTIONS({ commit }) {
    commit('UPDATE_OPTIONS', await fetchOptions(this));
  },
  async FETCH_PAGES({ commit }) {
    commit('UPDATE_PAGES', await fetchPages(this));
  },
  async nuxtServerInit({ dispatch }) {
    await Promise.all([
      dispatch('FETCH_OPTIONS'),
      dispatch('FETCH_PAGES')
    ]);
  }
};
