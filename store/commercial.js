import { fetchMassMedia, fetchReview, fetchProject, fetchAppeal } from '~/api/commercial';

export const state = () => ({
  mass_media: null,
  project: null,
  appeal: null,
  review: null
});

export const mutations = {
  UPDATE_MASS_MEDIA(state, payload) {
    state.mass_media = payload;
  },
  UPDATE_PROJECT(state, payload) {
    state.project = payload;
  },
  UPDATE_APPEAL(state, payload) {
    state.appeal = payload;
  },
  UPDATE_REVIEW(state, payload) {
    state.review = payload;
  }
};

export const actions = {
  async FETCH_MASS_MEDIA({ commit }) {
    await commit('UPDATE_MASS_MEDIA', await fetchMassMedia(this));
  },
  async FETCH_REVIEW({ commit }) {
    await commit('UPDATE_REVIEW', await fetchReview(this));
  },
  async FETCH_PROJECT({ commit }) {
    await commit('UPDATE_PROJECT', await fetchProject(this));
  },
  async FETCH_APPEAL({ commit }) {
    await commit('UPDATE_APPEAL', await fetchAppeal(this));
  }
};
