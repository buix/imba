export const state = () => ({
  multi_title: false,
  multi_line: false
});

export const mutations = {
  UPDATE_MULTI_LINE(state, payload) {
    state.multi_line = payload;
  },
  UPDATE_MULTI_TITLE(state, payload) {
    state.multi_title = payload;
  }
};
