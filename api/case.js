import { interpretResponse, interpretPaginatedResponse } from '~/utils/data';
import { format } from 'date-fns';

export const interpretCase = (model) => {
  return {
    id: model.id,
    title: model.title,
    description: model.customer_name,
    annotation: model.task_name,
    content: model.content ?? null,
    platform: model?.channel?.platform ?? null,
    date: format(Date.parse(model.created_at), 'dd.MM.yyyy'),
    tag: model.tags.map((tag) => tag.name).join(' '),
    image: model.poster.medium,
    url: model.slug
  };
};

export const asyncCase = (ctx) => {
  return ctx.$axios.get('/portfolios', {
    params: {
      per_page: 4,
      format_id: ctx.route.query.format,
      page: ctx.route.query.page
    }
  })
    .then(interpretPaginatedResponse)
    .then((response) => {
      return {
        cases: response.data.map(interpretCase),
        meta: response.meta
      };
    });
};

export const asyncPage = (ctx) => {
  return ctx.$axios.get(`/portfolios/${ctx.params.slug}`).then((response) => {
    return interpretResponse(response);
  }).then((model) => {
    return interpretCase(model);
  }).then((page) => {
    return {
      page
    };
  });
};
