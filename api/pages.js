import { interpretResponse } from '~/utils/data';

export const asyncPage = (ctx) => {
  return ctx.$axios.get(`/pages/${ctx.params.slug}`).then((response) => {
    return interpretResponse(response);
  }).then((model) => {
    if (model.content) {
      return model;
    }
    return ctx.error({ statusCode: 404, message: 'Not Found' });
  }).catch((e) => {
    return ctx.error({ statusCode: e.response?.status || 404, message: e.message });
  });
};
