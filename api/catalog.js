import { interpretPaginatedResponse, splitNumber, labelModel, trendValue, meshArray } from '~/utils/data';

const interpretTable = (model, index) => {
  model.views_count = +model.views_count || 0;
  model.subscriptions_count = +model.subscriptions_count || 0;

  const viewStats =
    model.views_count - (model?.prev_week_channel_stat?.views_count ?? 0);
  const subscribersStats =
    model.subscriptions_count - (model?.prev_week_channel_stat?.subscriptions_count ?? 0);

  return {
    id: model.id,
    number: index + 1,
    name: model.title,
    more: model.description,
    platform: model.platform,
    themes: model.subjects.map(labelModel),
    formats: model.formats.map(labelModel),
    views: {
      trend: trendValue(viewStats),
      current: splitNumber(model.views_count),
      week: splitNumber(viewStats)
    },
    subscribers: {
      trend: trendValue(subscribersStats),
      current: splitNumber(model.subscriptions_count),
      week: splitNumber(subscribersStats)
    }
  };
};

export const fetchData = (ctx) => {
  return ctx.$axios.get('/channels', {
    params: {
      platform: ctx.route.query.brand,
      subject_ids: meshArray(ctx.route.query.theme),
      format_ids: meshArray(ctx.route.query.format),
      page: ctx.route.query.page,
      subscriptions_count: meshArray(ctx.route.query.range)
    }
  })
    .then(interpretPaginatedResponse)
    .then((response) => {
      return {
        table: response.data.map(interpretTable),
        meta: response.meta
      };
    });
};
