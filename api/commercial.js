import { interpretResponse, parseVideoId } from '~/utils/data';

const interpretProject = (project) => {
  return {
    id: project.id,
    caption: project.customer_name,
    description: project.task_name,
    image: project.poster.square,
    url: project.slug
  };
};

const interpretReview = (review) => {
  return {
    id: review.id,
    title: review.name,
    description: review.company,
    annotation: review.text,
    image: review.image?.square,
    video: review.video_url ? `//www.youtube.com/embed/${parseVideoId(review.video_url)}` : null,
    fallback: review.image?.medium
  };
};

export const fetchMassMedia = (ctx) => {
  return ctx.$axios.get('/mass-media').then((response) => {
    return interpretResponse(response).map((card) => {
      return {
        id: card.id,
        caption: card.title,
        tag: card.author,
        image: card.poster.square,
        url: card.url
      };
    });
  });
};

export const fetchReview = (ctx) => {
  return ctx.$axios.get('/reviews').then((response) => {
    return interpretResponse(response).map(interpretReview);
  }).then((reviews) => {
    const rich = reviews.find((review) => {
      return review.video !== null;
    });

    if (rich) {
      const other = reviews.filter((review) => {
        return review !== rich;
      }).slice(0, 2);

      return {
        rich,
        other
      };
    }

    return {
      rich: reviews[0],
      other: reviews.slice(1, 3)
    };
  });
};

export const fetchProject = (ctx) => {
  return ctx.$axios.get('/portfolios?is_home=1').then((response) => {
    let cards = interpretResponse(response);

    if (cards.length > 0) {
      cards = cards.map(interpretProject);

      while (cards.length < 4) {
        cards.push(...cards);
      }

      return cards.slice(0, 4);
    }

    return [];
  });
};

export const fetchAppeal = (ctx) => {
  return ctx.$axios.get('/options').then((response) => {
    return interpretResponse(response)?.team_speech.value;
  });
};
