import { interpretResponse, interpretPaginatedResponse, labelModel } from '~/utils/data';
import { format } from 'date-fns';

export const interpretCard = (card) => {
  return {
    id: card.id,
    caption: card.title,
    tag: card.tags.map((model) => model.name).join(' '),
    image: card.poster.square,
    url: card.slug
  };
};

export const interpretSlider = (slider) => {
  return {
    id: slider.id,
    caption: slider.title,
    tag: slider.tags.map((model) => model.name).join(' '),
    image: slider.poster.medium,
    url: slider.slug
  };
};

export const interpretAside = (aside) => {
  return {
    id: aside.id,
    caption: aside.title,
    tag: aside.tags.map((model) => model.name).join(' '),
    image: aside.poster.medium,
    url: aside.slug
  };
};

const interpretBlog = (blog) => {
  return {
    id: blog.id,
    title: blog.title,
    content: blog.content,
    tags: blog.tags.map(labelModel),
    preview: blog.background.full
  };
};

const interpretComment = (comment) => {
  return {
    id: comment.id,
    caption: comment.name,
    description: comment.content,
    rating: comment.rating,
    date: format(Date.parse(comment.created_at), 'dd.MM.yyyy'),
  };
};

export const asyncBlog = (ctx) => {
  return Promise.all([
    ctx.$axios.get('blogs', {
      params: {
        page: ctx.route.query.page
      }
    }),
    ctx.$axios.get('blogs?position=is_top_slider'),
    ctx.$axios.get('blogs?position=is_bottom_slider'),
    ctx.$axios.get('blogs?position=is_siderbar')
  ]).then(([response, top, bottom, aside]) => {
    response = interpretPaginatedResponse(response);

    top = interpretResponse(top).map(interpretSlider);
    bottom = interpretResponse(bottom).map(interpretSlider);
    aside = interpretResponse(aside).map(interpretAside);

    const all = response.data.map(interpretCard);

    return {
      meta: response.meta,
      cards: {
        primary: all.slice(0, 6),
        secondary: all.slice(6, 8),
        other: all.slice(8),
        top,
        bottom,
        best: aside[0],
        aside: aside.slice(1)
      }
    };
  });
};

export const asyncPage = (ctx) => {
  return Promise.all([
    ctx.$axios.get(`/blogs/${ctx.params.slug}`),
    ctx.$axios.get(`/blogs/${ctx.params.slug}/top-comments`),
    ctx.$axios.get(`/blogs/${ctx.params.slug}/comments`)
  ]).then((responses) => {
    return responses.map(interpretResponse);
  }).then(([model, top, other]) => {
    return {
      page: interpretBlog(model),
      comments: {
        top: top.map(interpretComment),
        other: other.map(interpretComment)
      }
    };
  });
};
