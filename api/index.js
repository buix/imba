import { interpretResponse, labelModel } from '~/utils/data';

export const fetchThemes = (ctx) => {
  return ctx.$axios.get('/subjects').then((response) => {
    return interpretResponse(response).map(labelModel);
  });
};

export const fetchFormats = (ctx) => {
  return ctx.$axios.get('/formats').then((response) => {
    return interpretResponse(response).map(labelModel);
  });
};

export const fetchOptions = (ctx) => {
  return ctx.$axios.get('/options').then((response) => {
    return interpretResponse(response);
  });
};

export const fetchPages = (ctx) => {
  return ctx.$axios.get('/pages').then((response) => {
    return interpretResponse(response).map((page) => {
      return {
        id: page.id,
        caption: page.title,
        url: page.slug
      };
    });
  });
};
