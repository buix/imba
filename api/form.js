import { interpretResponse } from '~/utils/data';
import { interpretCard } from '~/api/blog';

export const submitForm = (ctx, params) => {
  return ctx.$axios.post('/feedback', params).then((response) => {
    return interpretResponse(response);
  });
};

export const submitComment = (ctx, params) => {
  return ctx.$axios.post(`/blogs/${ctx.params.slug}/comments`, params).then((response) => {
    return interpretResponse(response);
  });
};

export const fetchSearch = (ctx, param) => {
  return ctx.$axios.get('/blogs', {
    params: {
      s: param
    }
  }).then((response) => {
    return interpretResponse(response).map(interpretCard);
  });
};
