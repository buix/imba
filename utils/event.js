export const unwrap = (evt) => {
  evt = evt || globalThis.event;
  return evt.originalEvent || evt;
};

export const prevent = (evt) => {
  evt = unwrap(evt);

  if (!evt) {
    return;
  }

  if (evt.cancelable) {
    if (evt.preventDefault && !evt.defaultPrevented) {
      evt.preventDefault();
    } else {
      evt.returnValue = false;
    }
  }

  return false;
};

export const stop = (evt) => {
  evt = unwrap(evt);

  if (!evt) {
    return;
  }

  if (evt.stopImmediatePropagation) {
    evt.stopImmediatePropagation();
  }

  if (evt.stopPropagation) {
    evt.stopPropagation();
  }

  return true;
};

export const cancel = (evt) => {
  stop(evt);
  return prevent(evt);
};

export const GESTURE_EVENTS = ['dragstart', 'dragenter', 'gesturestart', 'gesturechange', 'MSGestureStart', 'MSGestureChange'];
export const ANIMATION_EVENTS = ['animationend', 'webkitAnimationEnd', 'mozAnimationEnd', 'MSAnimationEnd', 'oAnimationEnd'];
export const TRANSITION_EVENTS = ['transitionend', 'webkitTransitionEnd', 'mozTransitionEnd', 'MSTransitionEnd', 'oTransitionEnd'];
