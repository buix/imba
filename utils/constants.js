export const OPTIONS_MAP_SCHEME = {
  tg: 'https://telegram.me/',
  telephone: 'tel:',
  email: 'mailto:',
  inst: 'https://instagr.am/',
  vk: 'https://vk.com/'
};

export const OPTIONS_VALUE_WHY = {
  'customers__why_we__number': 0,
  'customers__why_we__channels_count': 1,
  'customers__why_we__views_count': 2,
  'customers__why_we__subscriptions_count': 3
};

export const OPTIONS_VALUE_PREPARE = {
  'bloggers__requirements__online': 0,
  'bloggers__requirements__month': 1,
  'bloggers__requirements__views_count': 2,
  'bloggers__requirements__solo': 3
};

export const OPTIONS_VALUE_WORK = {
  'bloggers__stats__campaign': 0,
  'bloggers__stats__coverage': 1,
  'bloggers__stats__earned': 2,
  'bloggers__stats__happy': 3
};

export const FORM_COMMENT = {
  content: '',
  name: '',
  email: '',
  company: '',
  rating: 4
};
export const FORM_QUESTION = {
  question: '',
  name: '',
  email: '',
  phone_number: ''
};

