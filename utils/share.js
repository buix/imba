export const WINDOW_PARAMS = 'noopener=yes,noreferrer=yes';

const SHARE_SCHEME = {
  fb: (title, url) => `https://www.facebook.com/sharer/sharer.php?u=${url}`,
  twitter: (title, url) => `https://twitter.com/intent/tweet/?text=${title}&url=${url}`,
  mail: (title, url) => `mailto:?to=&body=${title}%20${url}`,
  tg: (title, url) => `https://telegram.me/share?text=${title}&url=${url}`,
  vk: (title, url) => `http://vk.com/share.php?title=${title}&url=${url}`,
  linkedin: (title, url) => `https://www.linkedin.com/shareArticle?url=${url}&mini=true`
};

export const AVAILABLE = ['vk', 'tg', 'twitter', 'fb', 'mail', 'linkedin'];

export const share = (type, title, url) => {
  const by = SHARE_SCHEME[type](title, url);
  const sharer = globalThis.open(by, '_blank', WINDOW_PARAMS);
  if (sharer) {
    if (sharer.focus) {
      try {
        sharer.focus();
      } catch { }
    }
  }
};
