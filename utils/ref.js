export const fromRef = (ref) => {
  if (ref.value) {
    ref = ref.value;
  }
  return ref ? (ref.elm ?? ref.$el ?? ref) : null;
};
