import memoize from 'lodash/memoize';

export const interpretResponse = (response) => {
  while (response.data) {
    response = response.data;
  }
  return response;
};

export const interpretPaginatedResponse = (response) => {
  while (response.data) {
    if (response.meta) {
      return response;
    }

    response = response.data;
  }
  return response;
};

export const interpretAsync = (ctx) => {
  const state = ctx.ssrContext?.nuxt ?? ctx.root.context?.nuxtState;
  return state?.data?.[0];
};

export const interpretPagination = (ctx) => {
  return +(ctx.root.context.route.query?.page ?? 1);
};

export const meshArray = (val, fallback = null) => {
  if (!val) {
    return fallback;
  }

  if (Array.isArray(val)) {
    return val;
  }

  if (typeof val === 'object') {
    return Array.form(val);
  }

  return [val];
};

export const clearLink = (link) => {
  return link.replace(/(^[@/]+)|[():-\s]+/g, '');
};

export const parseVideoId = (any) => {
  const result = /(?:\/|v=|^)([a-z0-9_-]{11})(?:\?|&|$)/gi.exec(String(any).replace(/\s/g, ''));
  return result && result[1];
};

export const splitNumber = memoize((number) => {
  if (!number) {
    return ['0'];
  }

  const arr = [];
  let an = Math.abs(number);
  if (an > 9999) {
    let part;
    while (an > 999) {
      part = Math.floor(an % 1000);
      if (part < 10) {
        arr.push(`00${part}`);
      } else if (part < 100) {
        arr.push(`0${part}`);
      } else {
        arr.push(`${part}`);
      }
      an = Math.floor(an / 1000);
    }
  }
  arr.push(an);

  return arr.reverse();
});

const polyfillIntlNumberFormat = (n) => {
  return splitNumber(n).join(' ');
};

export const formatNumber = (() => {
  // nodejs does not support ru-RU
  if ((process.client || process.browser) && 'Intl' in globalThis) {
    if (Intl.NumberFormat.supportedLocalesOf('ru-RU')) {
      const provider = new Intl.NumberFormat('ru-RU');
      return (n) => {
        const result = provider.format(n);
        if (result.length === 5) { // BUG: 1000 -> 1 000 -> 1000
          return result.replace(' ', '');
        }
        return result;
      };
    }
  }

  return polyfillIntlNumberFormat;
})();

export const formatInnerNumber = (str) => {
  return str.replace(/[0-9]+/, formatNumber);
};

export const clearNumber = (value) => {
  return Number(value.replace(/\s*/g, ''));
};

export const labelModel = (model) => {
  model.label = model.title || model.name;
  return model;
};

export const trendValue = (value) => {
  if (value === 0) {
    return 'default';
  }

  if (value > 0) {
    return 'positive';
  }

  return 'negative';
};

export const convertPercentToGrade = (value, total) =>
  ((10 * value) / total + 0.5) | 0;

export const createPercentPattern = (value, total) => {
  const pattern = [0, 0, 0, 0, 0];

  let halves = convertPercentToGrade(+value, +total);
  let i = 0;

  while (halves >= 2) {
    pattern[i++] = 2;
    halves -= 2;
  }

  if (halves) {
    pattern[i] = halves;
  }

  return pattern;
};
