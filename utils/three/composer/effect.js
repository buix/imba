import { LinearFilter, RGBFormat, WebGLRenderTarget, PlaneGeometry, OrthographicCamera, Mesh, Scene } from 'three/build/three.module';
import { MaskPass, ClearMaskPass } from '~/utils/three/pass/mask';
import { ShaderPass } from '~/utils/three/pass/shader';
import { CopyShader } from '~/utils/three/shader/copy';

import isNil from 'lodash/isNil';

class EffectComposer {
  constructor(renderer, renderTarget) {
    this.renderer = renderer;
    if (isNil(renderTarget)) {
      const width = globalThis.innerWidth || 1;
      const height = globalThis.innerHeight || 1;
      const parameters = { minFilter: LinearFilter, magFilter: LinearFilter, format: RGBFormat, stencilBuffer: false };
      renderTarget = new WebGLRenderTarget(width, height, parameters);
    }
    this.renderTarget1 = renderTarget;
    this.renderTarget2 = renderTarget.clone();
    this.writeBuffer = this.renderTarget1;
    this.readBuffer = this.renderTarget2;
    this.passes = [];
    this.copyPass = new ShaderPass(CopyShader);
  }
  swapBuffers() {
    const tmp = this.readBuffer;
    this.readBuffer = this.writeBuffer;
    this.writeBuffer = tmp;
  }
  addPass(pass) {
    this.passes.push(pass);
  }
  insertPass(pass, index) {
    this.passes.splice(index, 0, pass);
  }
  render(delta) {
    this.writeBuffer = this.renderTarget1;
    this.readBuffer = this.renderTarget2;
    const il = this.passes.length;
    let maskActive = false;
    let pass;
    for (let i = 0; i < il; i ++) {
      pass = this.passes[i];
      if (!pass.enabled) {
        continue;
      }
      pass.render(this.renderer, this.writeBuffer, this.readBuffer, delta, maskActive);
      if (pass.needsSwap) {
        if (maskActive) {
          const context = this.renderer.context;
          context.stencilFunc(context.NOTEQUAL, 1, 0xffffffff);
          this.copyPass.render(this.renderer, this.writeBuffer, this.readBuffer, delta);
          context.stencilFunc(context.EQUAL, 1, 0xffffffff);
        }
        this.swapBuffers();
      }
      if (pass instanceof MaskPass) {
        maskActive = true;
      } else if (pass instanceof ClearMaskPass) {
        maskActive = false;
      }
    }
  }
  reset(renderTarget) {
    if (isNil(renderTarget)) {
      renderTarget = this.renderTarget1.clone();
      renderTarget.width = globalThis.innerWidth;
      renderTarget.height = globalThis.innerHeight;
    }
    this.renderTarget1 = renderTarget;
    this.renderTarget2 = renderTarget.clone();
    this.writeBuffer = this.renderTarget1;
    this.readBuffer = this.renderTarget2;
  }
  setSize(width, height) {
    const renderTarget = this.renderTarget1.clone();
    renderTarget.width = width;
    renderTarget.height = height;
    this.reset(renderTarget);
  }
}

EffectComposer.camera = new OrthographicCamera(-1, 1, 1, -1, 0, 1);
EffectComposer.quad = new Mesh(new PlaneGeometry(2, 2), null);
EffectComposer.scene = new Scene();
EffectComposer.scene.add(EffectComposer.quad);

export { EffectComposer };
