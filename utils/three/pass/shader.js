import { UniformsUtils, ShaderMaterial } from 'three/build/three.module';
import { EffectComposer } from '~/utils/three/composer/effect';

import isNil from 'lodash/isNil';

class ShaderPass {
  constructor(shader, textureID) {
    this.textureID = !isNil(textureID) ? textureID : 'tDiffuse';
    this.uniforms = UniformsUtils.clone(shader.uniforms);
    this.material = new ShaderMaterial({
      uniforms: this.uniforms,
      vertexShader: shader.vertexShader,
      fragmentShader: shader.fragmentShader
    });
    this.renderToScreen = false;
    this.enabled = true;
    this.needsSwap = true;
    this.clear = false;
  }
  render (renderer, writeBuffer, readBuffer) {
    if (this.uniforms[this.textureID]) {
      this.uniforms[this.textureID].value = readBuffer;
    }
    EffectComposer.quad.material = this.material;
    if (this.renderToScreen) {
      renderer.render(EffectComposer.scene, EffectComposer.camera);
    } else {
      renderer.render(EffectComposer.scene, EffectComposer.camera, writeBuffer, this.clear);
    }
  }
}

export { ShaderPass };
