import { Color } from 'three/build/three.module';

import isNil from 'lodash/isNil';

class RenderPass {
  constructor(scene, camera, overrideMaterial, clearColor, clearAlpha) {
    this.scene = scene;
    this.camera = camera;
    this.overrideMaterial = overrideMaterial;
    this.clearColor = clearColor;
    this.clearAlpha = !isNil(clearAlpha) ? clearAlpha : 1;
    this.oldClearColor = new Color();
    this.oldClearAlpha = 1;
    this.enabled = true;
    this.clear = true;
    this.needsSwap = false;
  }
  render (renderer, _, readBuffer) {
    this.scene.overrideMaterial = this.overrideMaterial;
    if (this.clearColor) {
      this.oldClearColor.copy(renderer.getClearColor());
      this.oldClearAlpha = renderer.getClearAlpha();
      renderer.setClearColor(this.clearColor, this.clearAlpha);
    }
    renderer.render(this.scene, this.camera, readBuffer, this.clear);
    if (this.clearColor) {
      renderer.setClearColor(this.oldClearColor, this.oldClearAlpha);
    }
    this.scene.overrideMaterial = null;
  }
}

export { RenderPass };
