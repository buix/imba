import { LinearFilter, RGBFormat, WebGLRenderTarget, MeshDepthMaterial, UniformsUtils, ShaderMaterial } from 'three/build/three.module';
import { BokehShader } from '~/utils/three/shader/bokeh';
import { EffectComposer } from '~/utils/three/composer/effect';

import isNil from 'lodash/isNil';

class BokehPass {
  constructor(scene, camera, params) {
    this.scene = scene;
    this.camera = camera;
    const focus = !isNil(params.focus) ? params.focus : 1.0;
    const aspect = !isNil(params.aspect) ? params.aspect : camera.aspect;
    const aperture = !isNil(params.aperture) ? params.aperture : 0.025;
    const maxblur = !isNil(params.maxblur) ? params.maxblur : 1.0;
    const width = params.width || globalThis.innerWidth || 1;
    const height = params.height || globalThis.innerHeight || 1;
    this.renderTargetColor = new WebGLRenderTarget(width, height, {
      minFilter: LinearFilter,
      magFilter: LinearFilter,
      format: RGBFormat
    });
    this.renderTargetDepth = this.renderTargetColor.clone();
    this.materialDepth = new MeshDepthMaterial();
    const bokehShader = BokehShader;
    const bokehUniforms = UniformsUtils.clone(bokehShader.uniforms);
    bokehUniforms.tDepth.value = this.renderTargetDepth;
    bokehUniforms.focus.value = focus;
    bokehUniforms.aspect.value = aspect;
    bokehUniforms.aperture.value = aperture;
    bokehUniforms.maxblur.value = maxblur;
    this.materialBokeh = new ShaderMaterial({
      uniforms: bokehUniforms,
      vertexShader: bokehShader.vertexShader,
      fragmentShader: bokehShader.fragmentShader
    });
    this.uniforms = bokehUniforms;
    this.enabled = true;
    this.needsSwap = false;
    this.renderToScreen = false;
    this.clear = false;
  }
  render(renderer, writeBuffer, readBuffer) {
    const composer = EffectComposer;
    composer.quad.material = this.materialBokeh;
    this.scene.overrideMaterial = this.materialDepth;
    renderer.render(this.scene, this.camera, this.renderTargetDepth, true);
    this.uniforms.tColor.value = readBuffer;
    if (this.renderToScreen) {
      renderer.render(composer.scene, composer.camera);
    } else {
      renderer.render(composer.scene, composer.camera, writeBuffer, this.clear);
    }
    this.scene.overrideMaterial = null;
  }
}

export { BokehPass };
