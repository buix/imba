import { Vector3 } from 'three/build/three.module';
import { VERTEX, FRAGMENT } from '~/utils/three/shared/color';

export const ColorCorrectionShader = {
  uniforms: {
    'tDiffuse': {
      type: 't',
      value: null
    },
    'powRGB': {
      type: 'v3',
      value: new Vector3(2, 2, 2)
    },
    'mulRGB': {
      type: 'v3',
      value: new Vector3(1, 1, 1)
    }
  },
  vertexShader: VERTEX,
  fragmentShader: FRAGMENT
};
