import { VERTEX, FRAGMENT } from '~/utils/three/shared/copy';

export const CopyShader = {
  uniforms: {
    'tDiffuse': {
      type: 't',
      value: null
    },
    'opacity': {
      type: 'f',
      value: 1
    }
  },
  vertexShader: VERTEX,
  fragmentShader: FRAGMENT
};
