import { VERTEX, FRAGMENT } from '~/utils/three/shared/bokeh';

export const BokehShader = {
  uniforms: {
    'tColor': {
      type: 't',
      value: null
    },
    'tDepth': {
      type: 't',
      value: null
    },
    'focus': {
      type: 'f',
      value: 1.0
    },
    'aspect': {
      type: 'f',
      value: 1.0
    },
    'aperture': {
      type: 'f',
      value: 0.025
    },
    'maxblur': {
      type: 'f',
      value: 1.0
    }
  },
  vertexShader: VERTEX,
  fragmentShader: FRAGMENT
};
