import {
  PerspectiveCamera,
  Vector3,
  Scene,
  WebGLRenderer,
  TextureLoader,
  VideoTexture,
  RepeatWrapping,
  LinearFilter,
  MeshBasicMaterial,
  Mesh,
  SphereGeometry,
  IcosahedronGeometry,
  ShaderMaterial
} from 'three/build/three.module';

import { RenderPass } from '~/utils/three/pass/render';
import { BokehPass } from '~/utils/three/pass/bokeh';
import { ShaderPass } from '~/utils/three/pass/shader';

import { EffectComposer } from '~/utils/three/composer/effect';

import { ColorCorrectionShader } from '~/utils/three/shader/color';

import { VERTEX, FRAGMENT } from '~/utils/three/shared/eye';

class Eye {
  constructor() {
    this.frame = null;

    this.epsilon = Number.EPSILON || (2 ** -52);

    this.backMat = null;
    this.material = null;
    this.video = null;
    this.camera = null;
    this.scene = null;
    this.renderer = null;
    this.backGeo = null;
    this.eyeGeoL = null;

    this.mouseX = 0;
    this.mouseY = 0;
    this.realX = 0;
    this.realY = 0;
    this.tempX = 0;
    this.tempY = 0;

    this.width = globalThis.innerWidth;
    this.height = globalThis.innerHeight - 96;

    this.windowHalfX = this.width / 2;
    this.windowHalfY = this.height / 2;

    this.lon = 0;

    this.phi = 0;
    this.theta = 0;
    this.lat = 15;

    this.eyeController = {
      pupil_size: 0.2,
      iris_tex_start: 0.009,
      iris_tex_end: 0.13,
      iris_border: 0.001,
      iris_size: 0.52,
      iris_edge_fade: 0,
      iris_inset_depth: 0,
      sclera_tex_scale: 0,
      sclera_tex_offset: 1,
      ior: 2,
      refract_edge_softness: 0.1,
      iris_texture_curvature: 0.51,
      arg_iris_shading_curvature: 0.51,
      tex_U_offset: 0.25,
      cornea_bump_amount: 0.1,
      cornea_bump_radius_mult: 0.9,
      iris_normal_offset: 0.001,
      cornea_density: 0.001,
      bump_texture: 0.3,
      catshape: false,
      col_texture: true
    };
  }
  init({ canvas, video, camera }) {
    try {
      const promise = video.play();
      if ('catch' in promise) {
        promise.catch(() => { /* ignore */ });
      }
    } catch { /* ignore */ }

    this.camera = new PerspectiveCamera(45, this.width / this.height, 1, 3000);
    this.camera.position.z = 90;
    this.camera.target = new Vector3(camera.x, camera.y, camera.z);

    this.scene = new Scene();

    this.renderer = new WebGLRenderer({
      antialias: false,
      canvas
    });
    this.renderer.setSize(this.width, this.height);

    const textureEnvRefl_A = new TextureLoader().load(require('~/assets/textures/env/alex_refl_v01.jpg'));
    const textureEnvDiff_A = new TextureLoader().load(require('~/assets/textures/env/alex_diff_v01.jpg'));
    const textureEnvBack_A = new TextureLoader().load(require('~/assets/textures/env/alex_bg_v01.jpg'));

    const textureEyeColor_A = new VideoTexture(video);
    textureEyeColor_A.wrapS = RepeatWrapping;
    textureEyeColor_A.minFilter = textureEyeColor_A.magFilter = LinearFilter;
    const textureEyeNormal_A = new TextureLoader().load(require('~/assets/textures/eye/eye2_normals_v03.png'));
    textureEyeNormal_A.wrapS = RepeatWrapping;
    textureEyeNormal_A.minFilter = textureEyeNormal_A.magFilter = LinearFilter;

    const textureEyeRefract_C = new TextureLoader().load(require('~/assets/textures/eye/eye4_refract_v01.png'));

    this.backMat = new MeshBasicMaterial({ map: textureEnvBack_A });
    this.backGeo = new Mesh(new SphereGeometry(2000, 64, 32), this.backMat);
    this.backGeo.scale.z = -1;
    this.scene.add(this.backGeo);

    const uniforms = {
      texEyeCol: { type: 't', value: textureEyeColor_A },
      texEyeNrm: { type: 't', value: textureEyeNormal_A },
      texEnvRfl: { type: 't', value: textureEnvRefl_A },
      texEnvDif: { type: 't', value: textureEnvDiff_A },
      texEnvRfr: { type: 't', value: textureEyeRefract_C },
      pupil_size: { type: 'f', value: this.eyeController.pupil_size },
      iris_tex_start: { type: 'f', value: this.eyeController.iris_tex_start },
      iris_tex_end: { type: 'f', value: this.eyeController.iris_tex_end },
      iris_border: { type: 'f', value: this.eyeController.iris_border },
      iris_size: { type: 'f', value: this.eyeController.iris_size },
      iris_edge_fade: { type: 'f', value: this.eyeController.iris_edge_fade },
      iris_inset_depth: { type: 'f', value: this.eyeController.iris_inset_depth },
      sclera_tex_scale: { type: 'f', value: this.eyeController.sclera_tex_scale },
      sclera_tex_offset: { type: 'f', value: this.eyeController.sclera_tex_offset },
      ior: { type: 'f', value: this.eyeController.ior },
      refract_edge_softness: { type: 'f', value: this.eyeController.refract_edge_softness },
      iris_texture_curvature: { type: 'f', value: this.eyeController.iris_texture_curvature },
      arg_iris_shading_curvature: { type: 'f', value: this.eyeController.arg_iris_shading_curvature },
      tex_U_offset: { type: 'f', value: this.eyeController.tex_U_offset },
      cornea_bump_amount: { type: 'f', value: this.eyeController.cornea_bump_amount },
      cornea_bump_radius_mult: { type: 'f', value: this.eyeController.cornea_bump_radius_mult },
      iris_normal_offset: { type: 'f', value: this.eyeController.iris_normal_offset },
      cornea_density: { type: 'f', value: this.eyeController.cornea_density },
      bump_texture: { type: 'f', value: this.eyeController.bump_texture },
      catshape: { type: 'i', value: false },
      cybshape: { type: 'f', value: false },
      col_texture: { type: 'i', value: true },
    };
    const geometry = new IcosahedronGeometry(24, 4);

    this.material = new ShaderMaterial({
      uniforms,
      vertexShader: VERTEX,
      fragmentShader: FRAGMENT
    });
    this.eyeGeoL = new Mesh(geometry, this.material);
    this.scene.add(this.eyeGeoL);

    this.initPostprocessing();

    this.renderer.autoClear = false;

    this.renderer.domElement.style.position = 'absolute';
    this.renderer.domElement.style.top = '0px';
    this.renderer.domElement.style.left = '0px';
  }
  onMouseMove(event) {
    this.realX = (event.clientX - this.windowHalfX) * 0.0015;
    this.realY = (event.clientY - this.windowHalfY) * 0.002;
  }
  onWindowResize() {
    this.windowHalfX = globalThis.innerWidth / 2;
    this.windowHalfY = globalThis.innerHeight / 2;
    this.width = globalThis.innerWidth;
    this.height = globalThis.innerHeight;
    this.camera.aspect = this.width / this.height;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(this.width, this.height);
  }
  initPostprocessing() {
    const renderPass = new RenderPass(this.scene, this.camera);

    const bokehPass = new BokehPass(this.scene, this.camera, {
      focus: 1.0,
      aperture: 0.025,
      maxblur: 1.0,
      width: this.width,
      height: this.height
    });
    bokehPass.renderToScreen = true;

    const colorCorrectionPass = new ShaderPass(ColorCorrectionShader);
    colorCorrectionPass.uniforms.powRGB.value = new Vector3(1.0, 1.0, 1.0);
    colorCorrectionPass.uniforms.mulRGB.value = new Vector3(1.0, 1.0, 1.0);

    const composer = new EffectComposer(this.renderer);
    composer.addPass(renderPass);
    composer.addPass(colorCorrectionPass);
    composer.addPass(bokehPass);
  }
  animate() {
    this.frame = globalThis.requestAnimationFrame(() => {
      this.animate();
    }, this.renderer.domElement);
    this.render();
  }
  destroy() {
    globalThis.cancelAnimationFrame(this.frame);

    this.renderer.dispose();

    this.backMat = null;
    this.material = null;
    this.video = null;
    this.camera = null;
    this.scene = null;
    this.renderer = null;
    this.backGeo = null;
    this.eyeGeoL = null;
  }
  render() {
    this.lat = Math.max(-85, Math.min(85, this.lat));
    this.phi = (90 - this.lat) * Math.PI / 180;
    this.theta = this.lon * Math.PI / 180;

    this.phi += 0.3;
    this.theta += (Math.PI / 2);

    this.camera.position.x = 90 * Math.sin(this.phi) * Math.cos(this.theta);
    this.camera.position.y = 90 * Math.cos(this.phi);
    this.camera.position.z = 90 * Math.sin(this.phi) * Math.sin(this.theta);

    this.camera.lookAt(this.camera.target);

    this.eyeGeoL.lookAt(this.camera.position);

    this.tempX = this.realX - this.mouseX;
    if (Math.abs(this.tempX) < this.epsilon) {
      this.mouseX = this.realX;
    } else {
      this.mouseX += this.tempX * 0.15;
    }

    this.tempY = this.realY - this.mouseY;
    if (Math.abs(this.tempX) < this.epsilon) {
      this.mouseY = this.realY;
    } else {
      this.mouseY += this.tempY * 0.15;
    }

    this.eyeGeoL.rotateOnAxis(new Vector3(1, 0, 0), this.mouseY);
    this.eyeGeoL.rotateOnAxis(new Vector3(0, 1, 0), this.mouseX);

    this.renderer.clear();
    this.renderer.render(this.scene, this.camera);
  }
}

export { Eye };
