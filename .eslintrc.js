module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  globals: {
    globalThis: false
  },
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2019,
    sourceType: 'module',
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    }
  },
  extends: [
    'eslint:recommended',
    'problems',
    'plugin:vue/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: [
    'import',
    'node',
    'nuxt',
    'promise',
    'vue'
  ],
  rules: {
    'semi': 2,
    'vue/no-v-html': 'off',
    'vue/require-prop-types': 'off',
    'vue/require-default-prop': 'off',
    'vue/order-in-components': 'off',
    'indent': [
      2,
      2,
      {
        'VariableDeclarator': 'first',
        'FunctionDeclaration': {
          'parameters': 'first'
        },
        'CallExpression': {
          'arguments': 'first'
        },
        'ArrayExpression': 1,
        'ObjectExpression': 1,
        'SwitchCase': 1
      }
    ],
    'array-bracket-spacing': [2, 'never'],
    'object-curly-spacing': [2, 'always', { 'objectsInObjects': false, 'arraysInObjects': false }],
    'computed-property-spacing': [2, 'never'],
    'space-in-parens': [2, 'never'],
    'comma-spacing': [2, { 'before': false, 'after': true }],
    'quotes': [2, 'single', { 'allowTemplateLiterals': true }],
    'no-undef-init': 2,
    'no-void': 2,
    'no-undefined': 2,
    'no-shadow-restricted-names': 2
  }
};
