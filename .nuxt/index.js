import Vue from 'vue'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '../layouts/error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_portalvue_5718d5d4 from 'nuxt_plugin_portalvue_5718d5d4' // Source: ./portal-vue.js (mode: 'all')
import nuxt_plugin_webfontloader_929d3e42 from 'nuxt_plugin_webfontloader_929d3e42' // Source: ./webfontloader.js (mode: 'client')
import nuxt_plugin_axios_044c69df from 'nuxt_plugin_axios_044c69df' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_nuxtsvgsprite_726a8b86 from 'nuxt_plugin_nuxtsvgsprite_726a8b86' // Source: ./nuxt-svg-sprite.js (mode: 'all')
import nuxt_plugin_global_f21f4e84 from 'nuxt_plugin_global_f21f4e84' // Source: ../plugins/global.js (mode: 'all')
import nuxt_plugin_requestpolyfillclient_c7befb3e from 'nuxt_plugin_requestpolyfillclient_c7befb3e' // Source: ../plugins/request-polyfill.client.js (mode: 'client')
import nuxt_plugin_intersectionobserverclient_6b6d967c from 'nuxt_plugin_intersectionobserverclient_6b6d967c' // Source: ../plugins/intersection-observer.client.js (mode: 'client')
import nuxt_plugin_composition_77f52d03 from 'nuxt_plugin_composition_77f52d03' // Source: ../plugins/composition.js (mode: 'all')
import nuxt_plugin_bus_74525326 from 'nuxt_plugin_bus_74525326' // Source: ../plugins/bus.js (mode: 'all')
import nuxt_plugin_optimizeeventclient_89e2e304 from 'nuxt_plugin_optimizeeventclient_89e2e304' // Source: ../plugins/optimize-event.client.js (mode: 'client')
import nuxt_plugin_touchable_562dd4b4 from 'nuxt_plugin_touchable_562dd4b4' // Source: ../plugins/touchable.js (mode: 'all')
import nuxt_plugin_textarea_01578172 from 'nuxt_plugin_textarea_01578172' // Source: ../plugins/textarea.js (mode: 'all')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

async function createApp (ssrContext) {
  const router = await createRouter(ssrContext)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Fix SSR caveat https://github.com/nuxt/nuxt.js/issues/3757#issuecomment-414689141
  const registerModule = store.registerModule
  store.registerModule = (path, rawModule, options) => registerModule.call(store, path, rawModule, Object.assign({ preserveState: process.client }, options))

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"htmlAttrs":{"x-ms-format-detection":"none","lang":"ru"},"title":"","meta":[{"http-equiv":"x-ua-compatible","content":"ie=edge"},{"name":"format-detection","content":"telephone=no"},{"name":"format-detection","content":"date=no"},{"name":"format-detection","content":"address=no"},{"name":"format-detection","content":"email=no"},{"name":"viewport","content":"width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0,minimal-ui"}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ffavicon.ico"},{"rel":"author","type":"text\u002Fplain","href":"\u002Fhumans.txt"},{"rel":"preconnect","href":"https:\u002F\u002Ffonts.googleapis.com"},{"rel":"dns-prefetch","href":"https:\u002F\u002Fstorage.googleapis.com"}],"style":[],"script":[]},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  const inject = function (key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Plugin execution

  if (typeof nuxt_plugin_portalvue_5718d5d4 === 'function') {
    await nuxt_plugin_portalvue_5718d5d4(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_webfontloader_929d3e42 === 'function') {
    await nuxt_plugin_webfontloader_929d3e42(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_044c69df === 'function') {
    await nuxt_plugin_axios_044c69df(app.context, inject)
  }

  if (typeof nuxt_plugin_nuxtsvgsprite_726a8b86 === 'function') {
    await nuxt_plugin_nuxtsvgsprite_726a8b86(app.context, inject)
  }

  if (typeof nuxt_plugin_global_f21f4e84 === 'function') {
    await nuxt_plugin_global_f21f4e84(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_requestpolyfillclient_c7befb3e === 'function') {
    await nuxt_plugin_requestpolyfillclient_c7befb3e(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_intersectionobserverclient_6b6d967c === 'function') {
    await nuxt_plugin_intersectionobserverclient_6b6d967c(app.context, inject)
  }

  if (typeof nuxt_plugin_composition_77f52d03 === 'function') {
    await nuxt_plugin_composition_77f52d03(app.context, inject)
  }

  if (typeof nuxt_plugin_bus_74525326 === 'function') {
    await nuxt_plugin_bus_74525326(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_optimizeeventclient_89e2e304 === 'function') {
    await nuxt_plugin_optimizeeventclient_89e2e304(app.context, inject)
  }

  if (typeof nuxt_plugin_touchable_562dd4b4 === 'function') {
    await nuxt_plugin_touchable_562dd4b4(app.context, inject)
  }

  if (typeof nuxt_plugin_textarea_01578172 === 'function') {
    await nuxt_plugin_textarea_01578172(app.context, inject)
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, () => {
        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from, next) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
