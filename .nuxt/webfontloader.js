
import WebFont from 'webfontloader'

const options = {"classes":false,"events":false,"custom":{"families":["Open Sans"],"urls":["https:\u002F\u002Ffonts.googleapis.com\u002Fcss2?family=Open+Sans:wght@400;700&display=block"]}}

WebFont.load(options)
