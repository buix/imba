import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _fbb4d430 = () => interopDefault(import('../pages/blog/index.vue' /* webpackChunkName: "pages/blog/index" */))
const _3326043f = () => interopDefault(import('../pages/blogger.vue' /* webpackChunkName: "pages/blogger" */))
const _195f47f6 = () => interopDefault(import('../pages/case/index.vue' /* webpackChunkName: "pages/case/index" */))
const _5032d734 = () => interopDefault(import('../pages/catalog.vue' /* webpackChunkName: "pages/catalog" */))
const _347e4ae6 = () => interopDefault(import('../pages/commercial.vue' /* webpackChunkName: "pages/commercial" */))
const _ff2216c0 = () => interopDefault(import('../pages/blog/_slug.vue' /* webpackChunkName: "pages/blog/_slug" */))
const _17a8a6ae = () => interopDefault(import('../pages/case/_slug.vue' /* webpackChunkName: "pages/case/_slug" */))
const _7f5f61df = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _7da8c097 = () => interopDefault(import('../pages/_slug.vue' /* webpackChunkName: "pages/_slug" */))
const _3947663c = () => interopDefault(import('./nuxt-svg-sprite-icons-list.vue' /* webpackChunkName: "" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/blog",
    component: _fbb4d430,
    pathToRegexpOptions: {"strict":true},
    name: "blog"
  }, {
    path: "/blogger",
    component: _3326043f,
    pathToRegexpOptions: {"strict":true},
    name: "blogger"
  }, {
    path: "/case",
    component: _195f47f6,
    pathToRegexpOptions: {"strict":true},
    name: "case"
  }, {
    path: "/catalog",
    component: _5032d734,
    pathToRegexpOptions: {"strict":true},
    name: "catalog"
  }, {
    path: "/commercial",
    component: _347e4ae6,
    pathToRegexpOptions: {"strict":true},
    name: "commercial"
  }, {
    path: "/blog/:slug",
    component: _ff2216c0,
    pathToRegexpOptions: {"strict":true},
    name: "blog-slug"
  }, {
    path: "/case/:slug",
    component: _17a8a6ae,
    pathToRegexpOptions: {"strict":true},
    name: "case-slug"
  }, {
    path: "/",
    component: _7f5f61df,
    pathToRegexpOptions: {"strict":true},
    name: "index"
  }, {
    path: "/:slug",
    component: _7da8c097,
    pathToRegexpOptions: {"strict":true},
    name: "slug"
  }, {
    path: "/_icons",
    component: _3947663c,
    name: "icons-list"
  }],

  parseQuery: function(query) {
      return require('qs').parse(query, { comma: true });
    },
  stringifyQuery: function(query) {
      const result = require('qs').stringify(query, { encode: false, arrayFormat: 'comma' });
      return result ? `?${result}` : '';
    },
  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
