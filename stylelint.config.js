module.exports = {
  plugins: [
    'stylelint-scss'
  ],
  extends: [
    'stylelint-config-recommended',
    'stylelint-config-recommended-scss'
  ],
  rules: {
    'no-empty-source': null,
    'no-duplicate-selectors': null,
    'no-descending-specificity': null,
    'property-no-unknown': null,
    'selector-pseudo-class-no-unknown': null,
    'font-family-no-duplicate-names': null
  }
};
