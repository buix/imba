export default {
  methods: {
    stripHtml(html) {
      const tmp = document.createElement('DIV');
      tmp.innerHTML = html;
      return tmp.textContent || tmp.innerText || '';
    }
  },
  computed: {
    seometa() {
      return [
        {
          hid: 'description',
          name: 'description',
          content: this.seoDesc
        },
        {
          property: 'og:title',
          content: this.seoTitle
        },
        {
          property: 'og:description',
          content: this.seoDesc
        },
        {
          property: 'og:image',
          content: this.seoImg
        },
        {
          property: 'twitter:card',
          content: 'summary'
        },
        {
          property: 'twitter:title',
          content: this.seoTitle
        },
        {
          property: 'twitter:description',
          content: this.seoDesc
        },
        {
          property: 'twitter:image',
          content: this.seoImg
        }
      ];
    }
  }
};